export default {
  'navbar': {
    'times': 'Zeiten',
    'weekly': 'Wöchentlich',
    'monthly': 'Monatlich',
    'contracts': 'Verträge',
    'sign-out': 'Ausloggen'
  },
  'timer': {
    'start-timer': 'Stoppuhr starten',
    'stop-timer': 'Stoppuhr beenden',
    'display': '{{duration}} Stunden'
  },
  'log-time': {
    'title': 'Zeit eintragen',
    'start': 'Anfang',
    'end': 'Ende',
    'comment': 'Kommentar',
    'contract': 'Vertrag',
    'cancel': 'Abbrechen',
    'action': 'Zeit eintragen',
    'success-message': 'Zeit eingetragen!',
    'error-message': 'Zeit eintragen fehlgeschlagen!'
  },
  'login': {
    'title': 'Anmelden',
    'username': 'Benutzername',
    'password': 'Passwort',
    'action': 'Anmelden',
    'success-message': 'Erfolgreich angemeldet!',
    'error-message': 'Anmelden fehlgeschlagen!'
  },
  'weekly': {
    'title': 'Woche {{from}} - {{to}}',
    'cw': 'KW {{week}}',
    'worked': 'Sie haben diese Woche {{hours}} Stunden gearbeitet.',
    'not-worked': 'Sie haben diese Woche noch nicht gearbeitet.'
  },
  'monthly': {
    'title': 'Monat {{from}} - {{to}}',
    'info': '{{name}} {{year}}',
    'worked': 'Sie haben diesen Monat {{hours}} Stunden gearbeitet.',
    'not-worked': 'Sie haben diesen Monat noch nicht gearbeitet.'
  },
  'period-table': {
    'start': 'Anfang',
    'end': 'Ende',
    'duration': 'Dauer',
    'contract': 'Vertrag',
    'comment': 'Kommentar',
    'entry': {
      'duration': '{{hours}} Stunden',
      'success-message': 'Zeit gelöscht!',
      'error-message': 'Zeit löschen fehlgeschlagen!',
      'edit-title': 'Zeit bearbeiten',
      'edit-action': 'Zeit bearbeiten',
      'delete-title': 'Zeit löschen',
      'delete-message': 'Möchten sie diese Zeit wirklich löschen?',
      'delete-action': 'Löschen'
    }
  },
  'contract': {
    'title': 'Verträge',
    'no-contracts': 'Sie haben bisher keine Verträge.',
    'add': 'Vertrag hinzufügen',
    'detail': {
      'title': 'Vertrag: {{name}}',
      'info': '{{from}} - {{to}}: {{hours}} Wochenstunden',
      'time': 'Arbeitszeit: '
    }
  },
  'contract-table': {
    'name': 'Name',
    'start': 'Anfang',
    'end': 'Ende',
    'hours-per-week': 'Wochenstunden',
    'statistics': 'Statistik'
  },
  'contract-time-info': {
    'text': '({{hours}}) Stunden'
  },
  'contract-dialog': {
    'name': 'Name',
    'start': 'Anfang',
    'end': 'Ende',
    'hours-per-week': 'Wochenstunden',
    'cancel': 'Abbrechen',
    'save': 'Speichern',
    'success-message': 'Vertrag gespeichert!',
    'error-message': 'Vertrag speichern fehlgeschlagen!'
  },
  'contract-edit-buttons': {
    'edit-title': 'Vertrag bearbeiten',
    'delete-title': 'Vertrag löschen',
    'delete-message': 'Möchten sie diesen Vertrag wirklich löschen? Dies löscht alle Zeiten, welche für diesen ' +
      'Vertrag eingetragen wurden.',
    'delete-confirm': 'Löschen',
    'success-message': 'Vertrag gelöscht!',
    'error-message': 'Vertrag löschen fehlgeschlagen!'
  },
  'add-contract-component': {
    'name': 'Vertrag {{number}}'
  },
  'confirm-dialog': {
    'cancel': 'Abbrechen'
  },
  'select-language': {
    'name': {
      'de': 'Deutsch',
      'en': 'Englisch'
    }
  },
  'itoast': {
    'success-title': 'Erfolg',
    'error-title': 'Fehler'
  },
  'errors': {
    'description': 'Dieses Feld',
    'inclusion': '{{description}} ist nicht in der liste',
    'exclusion': '{{description}} ist reserviert',
    'invalid': '{{description}} ist invalid',
    'confirmation': '{{description}} erfüllt {{on}} nicht',
    'accepted': '{{description}} muss akzeptiert werden',
    'empty': '{{description}} darf nicht leer sein',
    'blank': '{{description}} darf nicht leer sein',
    'present': '{{description}} muss leer sein',
    'collection': '{{description}} muss eine Kollektion sein',
    'singular': '{{description}} darf keine Kollektion sein',
    'tooLong': '{{description}} ist zu lang (Maximum ist {{max}} Zeichen)',
    'tooShort': '{{description}} ist zu kurz (Minimum ist {{min}} Zeichen)',
    'before': '{{description}} muss vor {{before}} sein',
    'after': '{{description}} muss nach {{after}} sein',
    'wrongDateFormat': '{{description}} muss folgendes Format haben: {{format}}',
    'wrongLength': '{{description}} hat die falsche Länge (sollte {{is}} Zeichen haben)',
    'notANumber': '{{description}} muss eine Zahl sein',
    'notAnInteger': '{{description}} muss eine Ganzzahl sein',
    'greaterThan': '{{description}} muss größer als {{gt}} sein',
    'greaterThanOrEqualTo': '{{description}} muss größer oder gleich als {{gte}} sein',
    'equalTo': '{{description}} muss gleich sein wie {{is}}',
    'lessThan': '{{description}} muss weniger als {{lt}} sein',
    'lessThanOrEqualTo': '{{description}} muss weniger oder gleich als {{lte}} sein',
    'otherThan': '{{description}} muss unterschiedlich zu {{value}} sein',
    'odd': '{{description}} muss ungerade sein',
    'even': '{{description}} muss gerade sein',
    'positive': '{{description}} muss positiv sein',
    'date': '{{description}} muss ein gültiges Datum sein',
    'email': '{{description}} muss eine gültige E-Mail Adresse sein',
    'phone': '{{description}} muss eine gültige Telefonnummer sein',
    'url': '{{description}} muss eine gültige URL sein',
    'no-whitespace-around': '{{description}} darf keine umschließenden Leerzeichen beinhalten'
  }
};
