import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import RedirectionRoute from 'timetracker/routes/redirection-route';
import {getCurrentMonthId} from 'timetracker/utils/month';

export default RedirectionRoute.extend(AuthenticatedRouteMixin, {
  redirectionTarget: 'monthly.month',

  getRedirectionParameter() {
    return getCurrentMonthId();
  }
});
