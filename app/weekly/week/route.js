import IntervalDetailRoute from 'timetracker/routes/interval-detail-route';

export default IntervalDetailRoute.extend({
  recordName: 'week'
});
