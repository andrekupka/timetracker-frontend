import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import RedirectionRoute from 'timetracker/routes/redirection-route';
import {getCurrentWeekId} from 'timetracker/utils/week';

export default RedirectionRoute.extend(AuthenticatedRouteMixin, {
  redirectionTarget: 'weekly.week',

  getRedirectionParameter() {
    return getCurrentWeekId();
  }
});
