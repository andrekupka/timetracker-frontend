# timetracker-frontend

The backend for `timetracker` written with ES6 and Ember.js.

## Backend

The corresponding frontend can be found [here](https://gitlab.com/freakout/timetracker-backend).
